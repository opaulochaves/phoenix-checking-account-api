defmodule Bank.Account.Operation do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bank.Account.Operation


  schema "account_operations" do
    field :account_number, :integer
    field :amount, :float
    field :date, :naive_datetime
    field :description, :string
    field :operation, :string

    timestamps()
  end

  @doc false
  def changeset(%Operation{} = operation, attrs) do
    operation
    |> cast(attrs, [:account_number, :operation, :description, :amount, :date])
    |> validate_required([:account_number, :operation, :description, :amount, :date])
    |> validate_number(:account_number, greater_than: 0)
    |> validate_number(:amount, greater_than: 0)
    |> validate_inclusion(:operation, ["deposit", "salary", "credit", "purchage", "withdrawal", "debit"])
  end
end

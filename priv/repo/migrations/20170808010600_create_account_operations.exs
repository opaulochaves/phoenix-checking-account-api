defmodule Bank.Repo.Migrations.CreateAccountOperations do
  use Ecto.Migration

  def change do
    create table(:account_operations) do
      add :account_number, :integer
      add :operation, :string
      add :description, :string
      add :amount, :float
      add :date, :naive_datetime

      timestamps()
    end

  end
end
